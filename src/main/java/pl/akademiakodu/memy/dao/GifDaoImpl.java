package pl.akademiakodu.memy.dao;

import org.springframework.stereotype.Component;
import pl.akademiakodu.memy.model.Category;
import pl.akademiakodu.memy.model.Gif;

import java.util.ArrayList;
import java.util.List;

@Component
public class GifDaoImpl implements GifDao {

    private static List<Gif> gifList = new ArrayList<>();

    static {
        gifList.add(new Gif(true, "../gifs/android-explosion.gif", "Jacek", CategoryDaoImpl.getCategories().get(0)));

        gifList.add(new Gif(false, "../gifs/ben-and-mike.gif", "Kazik", CategoryDaoImpl.getCategories().get(0)));
        gifList.add(new Gif(true, "../gifs/book-dominos.gif", "Mareczek", CategoryDaoImpl.getCategories().get(0)));
        gifList.add(new Gif(false, "../gifs/compiler-bot.gif", "DUPA", CategoryDaoImpl.getCategories().get(1)));
        gifList.add(new Gif(true, "../gifs/cowboy-coder.gif", "doopa208", CategoryDaoImpl.getCategories().get(1)));
        gifList.add(new Gif(false, "../gifs/infinite-andrew.gif", "Andżejczysta3", CategoryDaoImpl.getCategories().get(1)));
    }

    @Override
    public List<Gif> findAll() {
        return gifList;
    }


    @Override
    public List<Gif> findFavorites() {
        List<Gif> list = new ArrayList<>();
        for (Gif gif : gifList) {
            if (gif.isFavorite()) list.add(gif);
        }
        return list;
    }

    @Override
    public Gif findByName(String name) {
        for (Gif gif : gifList) {
            if (gif.getName().toLowerCase().equals(name.toLowerCase())) return gif;
        }
        return null;
    }

    public Gif searchByName(String name) {
        for (Gif gif : gifList) {
            if (gif.getName().toLowerCase().contains(name.toLowerCase())) return gif;
        }
        return null;
    }

    @Override
    public List<Gif> findByCategory(Category category) {
        Long id = category.getId();
        List<Gif> list = new ArrayList<>();
        for (Gif gif : gifList) {
            if (gif.getCategory().getId().compareTo(id) == 0) list.add(gif);
        }
        System.out.println(list);
        return list;
    }
}
