package pl.akademiakodu.memy.dao;

import pl.akademiakodu.memy.model.Category;
import pl.akademiakodu.memy.model.Gif;

import java.util.List;

public interface GifDao {
    List<Gif> findAll();
    List<Gif> findFavorites();
    Gif findByName(String name);
    List<Gif> findByCategory(Category category);
    Gif searchByName(String name);
}