package pl.akademiakodu.memy.dao;

import org.springframework.stereotype.Component;
import pl.akademiakodu.memy.model.Category;
import pl.akademiakodu.memy.model.Gif;

import java.util.ArrayList;
import java.util.List;


@Component
public class CategoryDaoImpl implements CategoryDao {
    private static List<Category> categories=new ArrayList<>();

    public static List<Category> getCategories() {
        return categories;
    }

    static {
        categories.add(new Category("ProgrammistMemes"));
        categories.add(new Category("AnimalMemes"));
    }

    @Override
    public List<Category> findAll(){
        return categories;
    }

    @Override
    public Category findById(Long id) {
        if(id>categories.size()-1) return null;
        return categories.get(id.intValue());
    }

    @Override
    public Category findByName(String name) {
        for(Category cat: categories){
            if(cat.getName().toLowerCase().equals(name.toLowerCase())) return cat;
        }
        return null;
    }

    public Category searchByName(String name) {
        for(Category cat: categories){
            if(cat.getName().toLowerCase().contains(name.toLowerCase())) return cat;
        }
        return null;
    }
}
