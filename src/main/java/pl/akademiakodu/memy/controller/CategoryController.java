package pl.akademiakodu.memy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.memy.dao.CategoryDao;
import pl.akademiakodu.memy.model.Category;

import java.util.List;


@RestController
public class CategoryController {

    @Autowired
    CategoryDao categoryDao;

    private static final String template = "%s!";

    @RequestMapping("api/categories/{id}")
    public Category category(@PathVariable Long id) {
        Category cat=categoryDao.findById(id);
        if(cat!=null) return cat;
        else return new Category(new Long(999999999),
                "BRAK KATEGORII");
    }

    @RequestMapping("api/categories")
    public List<Category> categories() {
        List<Category> categories=categoryDao.findAll();
        if(categories==null)
         {
            categories.add(new Category(new Long(999999999),
                    "BRAK KATEGORII"));
        }
        return categories;
    }
}