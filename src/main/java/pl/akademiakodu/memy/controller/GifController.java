package pl.akademiakodu.memy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.memy.dao.GifDao;
import pl.akademiakodu.memy.model.Gif;

import java.util.List;

@RestController
public class GifController {

    @Autowired
    GifDao gifDao;

    private static final String template = "%s!";

    @RequestMapping("api/gifs")
    public List<Gif> gifs() {
        List<Gif> gifs=gifDao.findAll();
        if(gifs==null)
        {
            gifs.add(new Gif("BRAK GIFÓW"));
        }
        return gifs;
    }
}