package pl.akademiakodu.memy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pl.akademiakodu.memy.dao.CategoryDao;
import pl.akademiakodu.memy.dao.GifDao;
import pl.akademiakodu.memy.model.Gif;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    GifDao gifDao;

    @Autowired
    CategoryDao categoryDao;


    @GetMapping("/")
    public String showHome(ModelMap modelMap){
        modelMap.addAttribute("gifs", gifDao.findAll());
        return "home";
    }

    @GetMapping("/favorites")
    public String showFavorites(ModelMap modelMap){
        modelMap.addAttribute("gifs", gifDao.findFavorites());
        return "favorites";
    }

    @GetMapping("/detail/{name}")
    public String showGif(@PathVariable String name, ModelMap modelMap){
        modelMap.addAttribute("gif", gifDao.findByName(name));
        return "gif-details";
    }

    @GetMapping("/categories")
    public String showGif(ModelMap modelMap){
        modelMap.addAttribute("categories", categoryDao.findAll());
        return "categories";
    }

    @GetMapping("/category/{id}")
    public String showGif(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("category",categoryDao.findById(id));
        modelMap.addAttribute("gifs", gifDao.findByCategory(categoryDao.findById(id)));
        return "category";
    }

    @GetMapping("/search")
    public String searchGif(@RequestParam String q, ModelMap modelMap){
        List<Gif> list=new ArrayList<>();
        if(categoryDao.searchByName(q)!=null) {
        if(gifDao.findByCategory(categoryDao.searchByName(q))!=null) {
            for (Gif gif : gifDao.findByCategory(categoryDao.searchByName(q))) {
                list.add(gif);
            }
        }}
        if(gifDao.searchByName(q)!=null) list.add(gifDao.searchByName(q));
        modelMap.addAttribute("gifs", list);
        return "searchgifs";
    }


}

