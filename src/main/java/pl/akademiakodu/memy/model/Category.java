package pl.akademiakodu.memy.model;

public class Category {
    private static Long counter=new Long(0);
    private Long id;
    private String name;

    public Category(String name) {
        this.name = name;
        this.id=counter;
        counter++;
    }

    public Category(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
