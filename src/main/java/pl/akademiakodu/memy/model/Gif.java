package pl.akademiakodu.memy.model;

public class Gif {
    private static Long counter=new Long(0);
    private Long id;
    private boolean favorite;
    private String name;
    private Category category;

    public Category getCategory() {
        return category;
    }

    public Gif(boolean favorite, String gifUrl, String name, Category category) {
        this.favorite = favorite;
        this.gifUrl = gifUrl;
        this.name=name;
        this.category=category;
        this.id=counter;
        counter++;
    }

    public Gif(String name) {
        this.name = name;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        counter++;
    }

    public Gif(boolean favorite, String gifUrl, String name) {
        this.favorite = favorite;
        this.gifUrl = gifUrl;
        this.name=name;
        this.id=counter;
        counter++;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    private String gifUrl;

    public Gif(String gifUrl, String name) {
        this.gifUrl = gifUrl;
        this.name=name;
        this.id=counter;
        counter++;
    }

    public String getGifUrl() {
        return gifUrl;
    }

    public void setGifUrl(String gifUrl) {
        this.gifUrl = gifUrl;
    }

    @Override
    public String toString() {
        return gifUrl;
    }
}
